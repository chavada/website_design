{
    # Theme information
    'name' : 'eCommerce 404',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Redirect to 404 Page when Targeted Page is not Found',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_base'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
