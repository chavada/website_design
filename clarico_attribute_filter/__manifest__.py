{
    # Theme information
    'name' : 'eCommerce Attribute Filter',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show Selected Attributes in Applied Filter Section',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_shop'
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Technical
    'installable': True,
}
