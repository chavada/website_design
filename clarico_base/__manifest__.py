{
    # Theme information
    'name' : 'eCommerce Base',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contains Common Design Styles for Theme Clarico',
    'description': """""",

    # Dependencies
    'depends': [
        'website_sale','website_blog', 'auth_signup',
    ],

    # Views
    'data': [
        'template/assets.xml',
    ],

    # Technical
    'installable': True,
    'application': False,
}
