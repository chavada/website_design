{
    # Theme information
    'name' : 'eCommerce Blog',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Easy & User Friendly Blogging Platform for your Online Store',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_layout'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
