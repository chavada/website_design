{
    # Theme information
    'name' : 'eCommerce Business Carousel',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contains Employee,Customer and Portfolio Carousels',
    'description': """""",

    # Dependencies
    'depends': [
	    'clarico_employee_carousel',
	    'clarico_customer_carousel',
    ],

    # Technical
    'installable': True,
}

