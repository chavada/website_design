{
    # Theme information
    'name' : 'eCommerce Carousel Editor',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Carousel RTE Editor',
    'description': """""",

    # Dependencies
    'depends': [
		'clarico_snippets',
    ],

    # Views
    'data': [
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
