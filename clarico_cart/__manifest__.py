{
    # Theme information
    'name' : 'eCommerce Cart',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Total Number of Products added to Cart',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_account','website_sale_delivery','website_sale_options','clarico_shop'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
