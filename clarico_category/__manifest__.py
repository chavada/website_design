{
    # Theme information
    'name' : 'eCommerce Category',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Website Category Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_carousel'
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Technical
    'installable': True,
}
