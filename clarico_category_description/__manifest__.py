{
    # Theme information
    'name' : 'eCommerce Category Description',
    'category' : 'Website',
    'version' : '1.0',
    'summary': '',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_shop'
    ],

    # Views
    'data': [
       'view/product_category.xml',
       'templates/template.xml',
       'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
