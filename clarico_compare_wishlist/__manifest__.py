{
    # Theme information
    'name' : 'eCommerce Compare Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Products into Wishlist from Compare Page',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_compare','clarico_wishlist',
    ],

    # Views
    'data': [
        'template/compare_theme_wishlist_template.xml',
    ],

    # Technical
    'installable': True,
}
