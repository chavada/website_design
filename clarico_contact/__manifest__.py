{
    # Theme information
    'name' : 'eCommerce Contact',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Contact us Page',
    'description': """""",

    # Dependencies
    'depends': [
        'website_crm','clarico_layout'
    ],

    # Views
    'data': [
        'template/template.xml',
        'template/assets.xml'
    ],

    # Technical
    'installable': True,
}
