{
    # Theme information
   
    'name': 'eCommerce Customize Color',
    'version': '11.0',
    'category': 'website',
    'summary': 'Custom Background Color to any section',
    'description': """""",
    
    # Dependencies
    
    'depends': ['clarico_layout'],
    
    # Views
    
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],
           
    # Technical 
    'installable': True,
    'auto_install': False,
}
