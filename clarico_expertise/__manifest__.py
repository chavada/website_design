{
    # Theme information
    'name' : 'eCommerce Expertise',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Expertise Snippet for Landing Pages',
    'description': """""",

    # Dependencies
    'depends': [
        'website_hr'
    ],

    # Views
    'data': [
        'security/ir.model.access.csv',
        'templates/template.xml',
        'templates/assets.xml',
        'views/res_company.xml',
    ],

    # Technical
    'installable': True,
}

