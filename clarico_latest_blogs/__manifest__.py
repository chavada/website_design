{
    # Theme information
    'name' : 'eCommerce Latest Blogs',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Website Blogs Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_blog',
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Technical
    'installable': True,
}
