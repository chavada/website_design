{
    # Theme information
    'name' : 'eCommerce Layout',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Navigate Online Store with 3 Unique Mega Menu Styles',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_base'
    ],

    # Views
    'data': [
        'template/template.xml',
        'template/assets.xml',
        'view/clarico_header.xml',
        'view/website_product_category.xml',
        'view/header_style.xml',
        'data/data.xml',
    ],

    # Technical
    'installable': True,
}
