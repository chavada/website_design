{
    # Theme information
    'name' : 'eCommerce Landing Page Layout1',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Landing Page Style 1',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_cms_blocks'
    ],

    # Views
    'data': [
        'templates/template.xml',
    ],

    # Technical
    'installable': True,
}
