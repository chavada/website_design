{
    # Theme information
    'name' : 'eCommerce Product',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'View Complete Product Information',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_base'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'view/product_video.xml',
        'view/product_short_description.xml'
    ],

    # Technical
    'installable': True,
}
