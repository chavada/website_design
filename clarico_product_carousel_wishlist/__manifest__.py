{
    # Theme information
    'name' : 'eCommerce Product Carousel Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Wishlist from Carousel',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_product_multi_carousel', 'clarico_wishlist'
    ],

    # Views
    'data': [
        'templates/template.xml', 
    ],

    # Technical
    'installable': True,
}
