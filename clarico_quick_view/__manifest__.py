{
    # Theme information
    'name' : 'eCommerce Quick View',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'View Complete Information about any Product in Lightbox',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_shop'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml'
    ],

    # Technical
    'installable': True,
}
