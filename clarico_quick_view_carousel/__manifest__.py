{
    # Theme information
    'name' : 'eCommerce Quick View Carousel',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Carousel from Quick View',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_quick_view','clarico_product_multi_carousel'
    ],

    # Views
    'data': [
        'template/template.xml',
    ],

    # Technical
    'installable': True,
}
