{
    # Theme information
    'name' : 'eCommerce Quick View Compare',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Compare from Quick View',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_quick_view','clarico_compare'
    ],

    # Views
    'data': [
        'template/template.xml',
    ],

    # Technical
    'installable': True,
}
