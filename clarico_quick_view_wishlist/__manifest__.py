{
    # Theme information
    'name' : 'eCommerce Quick View Wishlist',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Add Product to Compare from Wishlist',
    'description': """""",

    # Dependencies
    'depends': [
       'clarico_quick_view','clarico_wishlist'
    ],

    # Views
    'data': [
        'templates/template.xml'
    ],

    # Technical
    'installable': True,
}
