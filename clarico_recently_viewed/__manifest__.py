{
    # Theme information
    'name' : 'eCommerce Recently Viewed',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Recently Viewed Products',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_wishlist','clarico_product','clarico_cart'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml'
    ],

    # Technical
    'installable': True,
}
