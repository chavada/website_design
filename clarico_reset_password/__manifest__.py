{
    # Theme information
    'name' : 'eCommerce Reset Password',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Reset Password from Frontend',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_account'
    ],

    # Views
    'data': [
        'templates/template.xml',
    ],

    # Technical
    'installable': True,
}
