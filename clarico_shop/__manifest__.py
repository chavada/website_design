{
    # Theme information
    'name' : 'eCommerce Shop',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Showcase Products in Unique Style at your Online Store',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_layout'
    ],

    # Views
    'data': [
        'security/ir.model.access.csv', 
        'templates/template.xml',
        'view/product_template.xml',
        'view/product_label.xml',
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
