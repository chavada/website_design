{
    # Theme information
    'name' : 'eCommerce Shop Left Side Bar',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Category Page - Filters at Left',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_shop'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
