{
    # Theme information
    'name' : 'eCommerce Shop List',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Category Page - Showcase Products in List ',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_shop'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
