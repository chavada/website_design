{
    # Theme information
    'name' : 'eCommerce SignIn',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'User Sign In Page',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_layout'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'view/signin_img.xml',
    ],

    # Technical
    'installable': True,
}
