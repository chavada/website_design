{
    # Theme information
    'name' : 'eCommerce SignUp',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'User Sign Up Page',
    'description': """""",

    # Dependencies
    'depends': [
      'clarico_layout'
    ],

    # Views
    'data': [
        'templates/template.xml',
        'templates/assets.xml',
        'view/signup_img.xml'
    ],

    # Technical
    'installable': True,
}
