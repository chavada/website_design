{
    # Theme information
    'name' : 'eCommerce Similar Product',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'Show Similar Products',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_shop'
    ],

    # Views
    'data': [
        'template/assets.xml',
        'template/template.xml',
    ],

    # Technical
    'installable': True,
}
