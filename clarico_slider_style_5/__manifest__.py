{
    # Theme information
    'name' : 'eCommerce Slider Style 5',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
         'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/template.xml',  
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}

