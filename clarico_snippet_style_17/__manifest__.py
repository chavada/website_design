{
    # Theme information
    'name' : 'eCommerce Snippet Style 17',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets','clarico_expertise'
    ],

    # Views
    'data': [
        'templates/snippet_17.xml',  
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
