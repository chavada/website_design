{
    # Theme information
    'name' : 'eCommerce Snippet Style 18',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/snippet_18.xml',  
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
