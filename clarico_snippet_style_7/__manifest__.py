{
    # Theme information
    'name' : 'eCommerce Snippet Style 7',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Block',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/snippet_7.xml',  
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}
