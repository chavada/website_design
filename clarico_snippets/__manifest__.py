{
    # Theme information
    'name' : 'eCommerce Snippets',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'All Custom CMS Block Builders',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_layout'
    ],

    # Views
    'data': [
        'templates/snippets.xml', 
    ],

    # Technical
    'installable': True,
}
