{
    # Theme information
    'name' : 'Snippet Style 12',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/blank_space.xml', 
    ],

    # Technical
    'installable': True,
}
