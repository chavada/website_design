{
    # Theme information
    'name' : 'Snippet Style 13',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/snippet_13.xml',  
        'templates/assets.xml',  
    ],

    # Technical
    'installable': True,
}
