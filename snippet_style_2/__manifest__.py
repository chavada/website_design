{
    # Theme information
    'name' : 'Snippet Style 2',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'template/style_2.xml',  
        'template/timer.xml',
        'template/assets.xml',
    ],
    
    # Technical
    'installable': True,
}
