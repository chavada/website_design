{
    # Theme information
    'name' : 'Snippet Style 4',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/style_4.xml',  
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}

