{
    # Theme information
    'name' : 'Snippet Style 6',
    'category' : 'Website',
    'version' : '1.0',
    'summary': 'CMS Building Blocks',
    'description': """""",

    # Dependencies
    'depends': [
        'clarico_snippets'
    ],

    # Views
    'data': [
        'templates/style_6.xml',  
        'templates/assets.xml',
    ],

    # Technical
    'installable': True,
}

